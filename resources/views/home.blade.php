<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sistem Informasi Potensi dan Pengembangan Investasi</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="css/slick.css"/>
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css"/>
    <link rel="stylesheet" rel="stylesheet" href="css/styles.css">
</head>
<body>
    <div class="navbar-fixed">
        <nav class="red accent-3">
            <div class="nav-wrapper">
                <a href="#" class="brand-logo" title="Sistem Informasi Peta Administrasi & Peta Inovasi">
                    <span>SI-POPI</span>
                    <class class="childname">Sistem Informasi Potensi dan Pengembangan Investasi</span>
                </a>

                <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <!-- <ul class="right hide-on-med-and-down">
                    <li class="active"><a href="{{ url('/') }}">Home</a></li>
                    <li>
                        <a class="dropdown-trigger" href="#" data-target="dropdown-web">
                            Rute Potensi<i class="material-icons right">arrow_drop_down</i>
                        </a>
                    </li>
                </ul> -->
            </div>
        </nav>
    </div>
    <!-- <ul class="sidenav" id="nav-mobile">
        <li class="active"><a href="{{ url('/') }}">Home</a></li>
        <li>
            <a class="dropdown-trigger" href="#" data-target="dropdown-mobile">
                Rute Potensi<i class="material-icons right">arrow_drop_down</i>
            </a>
        </li>
    </ul>
    <ul id="dropdown-mobile" class="dropdown-content">
      <li><a href="{{ url('/rute/masyarakat') }}">Rute Kemasyarakatan</a></li>
      <li><a href="{{ url('/rute/wilayah') }}">Rute Kewilayahan</a></li>
      <li><a href="{{ url('/rute/pkk1') }}">Rute PKK 1</a></li>
      <li><a href="{{ url('/rute/pkk2') }}">Rute PKK 2</a></li>
    </ul>
    <ul id="dropdown-web" class="dropdown-content">
      <li><a href="{{ url('/rute/masyarakat') }}">Rute Kemasyarakatan</a></li>
      <li><a href="{{ url('/rute/wilayah') }}">Rute Kewilayahan</a></li>
      <li><a href="{{ url('/rute/pkk1') }}">Rute PKK 1</a></li>
      <li><a href="{{ url('/rute/pkk2') }}">Rute PKK 2</a></li>
    </ul> -->
    <div id="map-container">
        <div id="map"></div>
    </div>

    <div class="kategori hide-on-med-and-down">
        <h5>Pilih Kategori Potensi :</h5>
        <div class="row">
            <div class="input-field col s12">
                <select class="icons" name="kategori" id="kategori">
                    <option value="" disabled selected>Pilih Kategori</option>
                    @if($kategori)
                    @foreach($kategori as $kat)
                    <option value="{{ $kat->id }}" data-icon="{{ asset('images/icons/'.$kat->gambar_icon) }}">{{ $kat->nama }}</option>
                    @endforeach
                    @endif
                </select>
                <!-- <label>Pilih Kategori Potensinya</label> -->
            </div>
        </div>
      </div>

      <!-- Modal keterangan -->
    <div id="modals" class="modal">
        <div class="modal-content">
        <h5 id="info-header" class="center"></h5>
        <div id="info-detail"></div>
        </div>
    <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
    </div>
    <script src="js/jquery-3.3.1.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!-- <script src="js/home.js"></script> -->
    <script>
    function initMap() {
            var potensi = '{{ $potensi }}';
            var datapotensi = JSON.parse(potensi.replace(/&quot;/g,'"'));
            var marker, i, e;
            var latlang = {lat: -7.067248, lng: 110.379147};
            var infowdw = new google.maps.InfoWindow();
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 13,
              center: latlang
            });

            for (i = 0; i < datapotensi.length; i++) {
                // console.log(datapotensi[i])
                // var latlng = new google.maps.LatLng(datapotensi[i]['lat'], datapotensi[i]['long']);
                // var map = new google.maps.Map(document.getElementById('map'), {
                //     zoom: 14,
                //     center: latlng
                // });

                // var content = "<table class='striped'>";
                //     content	+= "<tr><th>Nama Tempat</th><td>"+datapotensi[i]['nama']+"</td></tr>";
                //     content	+= "<tr><th>Nama Pemilik</th><td>"+datapotensi[i]['pemilik']+"</td></tr>";
                //     content	+= "<tr><th>Alamat</th><td>"+datapotensi[i]['alamat']+"</td></tr>";
                //     content	+= "<tr><td colspan='3'><a href='#modals' class='modal-trigger'>Detail</a></td></tr>";
                //     content += "</table>";
                    // '<h4>'+datapotensi[i]['nama']+'</h4>' +
                    // '<div>' +
                    // '<p class="map-content">'+datapotensi[i]['keterangan']+'</p>' +
                    // '<img class="map-image" src="images/gunungpati.jpg" />' +
                    // '</div>';

                    marker = new google.maps.Marker({
						 	position: new google.maps.LatLng(datapotensi[i]['lat'], datapotensi[i]['long']),
						 	map: map,
                             zoom: 14,
						 	icon: "{{ asset('images/icons/') }}"+"/"+datapotensi[i]['kategori'][0]['gambar_icon']
						});
                //     var marker = new google.maps.Marker({
                //     position: latlng,
                //     map: map,
                //     title: datapotensi[i]['nama'],
				// 	icon: "{{ asset('images/icons/') }}"+"/"+datapotensi[i]['kategori'][0]['gambar_icon']

                // });
                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
							return function(){
                                var konten, lengkap;
								konten = "<table class='striped'>";
								konten	+= "<tr><th>Nama Tempat</th><td>"+datapotensi[i]['nama']+"</td></tr>";
								konten	+= "<tr><th>Nama Pemilik</th><td>"+datapotensi[i]['pemilik']+"</td></tr>";
								konten	+= "<tr><th>Alamat</th><td>"+datapotensi[i]['alamat']+"</td></tr>";
								konten	+= "<tr><td colspan='3'><a href='#modals' class='modal-trigger'>Detail</a></td></tr>";
								konten += "</table>";

								infowdw.setContent(konten);
								infowdw.open(map, marker);

                                lengkap 	= "<table class='table table-bordered table-striped'>";
								lengkap	+= "<tr><th>Nama Tempat</th><td>"+datapotensi[i]['nama']+"</td></tr>";
								lengkap	+= "<tr><th>Nama Pemilik</th><td>"+datapotensi[i]['pemilik']+"</td></tr>";
								lengkap	+= "<tr><th>Alamat</th><td>"+datapotensi[i]['alamat']+"</td></tr>";
								lengkap	+= "<tr><th>Keterangan</th><td>"+datapotensi[i]['keterangan']+"</td></tr>";
								lengkap	+= "<tr><th>Alamat</th><td>"+datapotensi[i]['alamat']+"</td></tr>";
								lengkap	+= "<tr><th>Foto</th><td><div class='info-slider'>";

                                for (e = 0; e < datapotensi[i]['gambar'].length; e++) {
                                    lengkap += "<div><img src='{{ asset('images/upload/') }}/"+datapotensi[i]['gambar'][e]['files']+"'></div>";

                                }
                                lengkap 	+= "</div></td></tr></table>";
                                
                                document.getElementById('info-detail').innerHTML = lengkap;
								document.getElementById('info-header').innerHTML = "INFORMASI "+datapotensi[i]['nama'].toUpperCase();
                            }
                        })(marker, i));
                

                // marker.addListener('click', function() {
                //     infowindow.open(map, marker);
                // });
            }
        }
    </script>
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyD1HOkkPnWMgiT1QxuGtknmpPlGc0Q3L1E&callback=initMap&libraries=places"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        


    // $('.info-slider').slick();
    $(".dropdown-trigger").dropdown();
        $('.sidenav').sidenav();
        $('select').formSelect();
        $('.modal').modal();
        // $('.carousel').carousel();
        // $('#slider').slider({full_width: true});

        $('#kategori').change(function() {
			var kategori = $('#kategori').val();
            var myLatLng = {lat: -7.067248, lng: 110.379147};
            var infowindow = new google.maps.InfoWindow();
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 13,
              center: myLatLng
            });
            $.ajax({
                type : 'POST',
                url : '{{ url("api/potensi") }}',
				dataType: "json",
				data : 'kategori='+ kategori,
				success : function(data){
                    var marker, i, e;

                    for (i = 0; i < data.length; i++) {

                        marker = new google.maps.Marker({
						 	position: new google.maps.LatLng(data[i]['lat'], data[i]['long']),
						 	map: map,
						 	icon: "{{ asset('images/icons/') }}"+"/"+data[i]['kategori'][0]['gambar_icon']
						});

                        google.maps.event.addListener(marker, 'click', (function(marker, i) {
							return function(){
                                var content, selengkapnya;
								content = "<table class='striped'>";
								content	+= "<tr><th>Nama Tempat</th><td>"+data[i]['nama']+"</td></tr>";
								content	+= "<tr><th>Nama Pemilik</th><td>"+data[i]['pemilik']+"</td></tr>";
								content	+= "<tr><th>Alamat</th><td>"+data[i]['alamat']+"</td></tr>";
								content	+= "<tr><td colspan='3'><a href='#modals' class='modal-trigger'>Detail</a></td></tr>";
								content += "</table>";
								infowindow.setContent(content);
								infowindow.open(map, marker);

                                selengkapnya 	= "<table class='table table-bordered table-striped'>";
								selengkapnya	+= "<tr><th>Nama Tempat</th><td>"+data[i]['nama']+"</td></tr>";
								selengkapnya	+= "<tr><th>Nama Pemilik</th><td>"+data[i]['pemilik']+"</td></tr>";
								selengkapnya	+= "<tr><th>Alamat</th><td>"+data[i]['alamat']+"</td></tr>";
								selengkapnya	+= "<tr><th>Keterangan</th><td>"+data[i]['keterangan']+"</td></tr>";
								selengkapnya	+= "<tr><th>Alamat</th><td>"+data[i]['alamat']+"</td></tr>";
								selengkapnya	+= "<tr><th>Foto</th><td><div class='info-slider'>";

                                for (e = 0; e < data[i]['gambar'].length; e++) {
                                    // selengkapnya += "<a class='carousel-item' href='#img"+e+"!'><img src='{{ asset('images/upload/') }}/"+data[i]['gambar'][e]['files']+"'></a>";
                                    selengkapnya += "<div><img src='{{ asset('images/upload/') }}/"+data[i]['gambar'][e]['files']+"'></div>";

                                }
								// selengkapnya	+= "<tr><th>Foto</th><td><img src='assets/upload/"+info['gambar']+"'></td></tr>";
                                selengkapnya 	+= "</div></td></tr></table>";

                                document.getElementById('info-detail').innerHTML = selengkapnya;
								document.getElementById('info-header').innerHTML = "INFORMASI "+data[i]['nama'].toUpperCase();
                            }
                        })(marker, i));
                    }
                }
            });
        });


    });
    </script>
</body>
</html>
