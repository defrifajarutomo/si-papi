<?php


Route::get('/', function () {
    $kategori = \App\Kategori::all();
    $potensi = \App\Potensi::with(['kategori', 'gambar'])->get()->toJson();
    // $potensi = $potensi->toJson(JSON_PRETTY_PRINT);

    return view('home', compact('kategori', 'potensi'));
});

Route::group(['prefix' => 'rute'], function() {
    Route::get('masyarakat', 'KunjunganController@masyarakat');
    Route::get('wilayah', 'KunjunganController@wilayah');
    Route::get('pkk1', 'KunjunganController@pkk1');
    Route::get('pkk2', 'KunjunganController@pkk2');
});

Route::resource('/ketik/potensi', 'PotensiController');
Route::resource('/ketik/kategori', 'KategoriController');
